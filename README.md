# Git repository template

## Description

**Git repository template** is a utility that allows  developers to easily follow Git source control rules defined on page  [Git commit message guideline](https://blog.kese.hu/2021/06/git-commit-message-guideline.html).

## Features

The Git template project includes the following independent functional supports:

1. Basic configurations for everyday Git tasks
2. Standardized repository README.md template for unified repository descriptions
3. Standardized commit messages templates for descriptive commit messages 
4. Commit message syntax highlight for two popular editors: nano and Sublime Text
5. Commit message validation for checking whether the commits follow the templates  

## Prerequisites

* **Operation system**: any
* **Framework**: -
* **Knowledge**: Git, PowerShell or Bash

The project is tested on [Git for windows](https://gitforwindows.org/) and on standard Git distro on Ubuntu (i.e. `sudo apt update && sudo apt install git`). For different platforms, a lot of Git Gui clients are [available](https://git-scm.com/downloads/guis/).

The following tools are recommended to the full-scaled functional support:

* **Git commit template syntax highlight**: nano or Sublime Text editors
* **Git commit message validation**: Perl 5.22+
* **[Git::Hooks](https://metacpan.org/pod/Git::Hooks)*: a framework for implementing Git hooks.

## Installation

To use the **Git repository template** repo, clone it and set it as a template directory in the Git user-specific configuration file (i.e. `.gitconfig`):
```
[init]
  templatedir = /path/to/cloned/repo/git-template
```

### Steps

The following steps are required to install the project:

1. Clone the `git-template` project:

```
git clone 
```

2. Copy and rename the repository template (in the repository root): 

   1. **In PowerShell**

      ```
      Copy-Item .\@git\repo-template\README-template.md .\README.md
      ```

   2. **In Bash**

      ```
      cp @git/repo-template/README-template.md ./README.md
      ```

3. Run the Git setup config script (in the repository root) - the scripts could be run several times. It alters only those settings which are controlled by the script. Other sections remain intact.

   1. **In PowerShell**

      ```
      .\SetupGitConfig.ps1
      ```

   2. **In Bash**

      ```
      ./setup_git_config.sh
      ```

The following conventions are independent each other. For the proposed settings below, helper scripts are available:

* `setup_git_config.sh` for bash
* `SetupGitConfig.ps1 ` for PowerShell 

**Note**: To execute the PowerShell script, run the following command in the Windows PowerShell:

```
"Set-ExecutionPolicy RemoteSigned" answer Y
```

### Basic Git configuration

For the most frequently  tasks run the next Git config commands in the the actual git repository directory (with `--global` switch for user-specific or with the `--global` switch for repository-specific changes):

**Auto-converting CRLF to LF line endings depending on Windows**

```
git config --global core.autocrlf true
```

**Enable long filenames on Windows**

```
git config --global core.longpaths true
```

**Automatically colors Git output**

```
git config --global ui.color true
```

**Set UTF-8 display for Git GUI differences window**

```
git config --global gui.encoding utf-8
```

**Autocorrect typos: it gives a clue about the most similar command (e.g. 'git stats' -> 'git status')**

```
git config --global help.autocorrect 1
```

**Abbreviation of the most frequent Git commands**

```
git config --global alias.st status
git config --global alias.ci commit
git config --global alias.co checkout
git config --globan alias.pom 'push origin master' # (*)
```

Autocomplete???

Git-extra: git local-commits; Git effort

**History graphs**

```
git config --global alias.hist 'log --all --graph --decorate --oneline' # (*)
```

**Pretty Git branch graphs**

```
git config --global alias.show-graph 'log --graph --abbrev-commit --pretty=oneline' # (*)
```

**Note**: The lines denoted by '(*)' are omitted from the Windows batch script. These could be executed by replacing single quotes with double quotes. (Windows accepts only double quotes, Linux and MacOS accepts single quotes.)

### Standardized repository README.md template

For the standardized repository description, copy the following repository README template (using [Markdown](https://www.markdownguide.org/basic-syntax/) syntax) file to the root of the actual git repository and rename it to README.md:

```
README-template.md
```

For editing the file, many Markdown editors are available freely, for example [Typora](https://typora.io/) (offline) and [Dilinger](https://dillinger.io/) (online). 

### Standardized commit messages

For standardized commit messages (that are based on [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)), copy the following directory (that contains commit type-specific commit message template files) to the root of the actual Git repository :

```
@commit-template/
```
There are nine different templates for different commit goals:

1. **build**: the commit changes that affect the build system or external dependencies (example scopes: gulp, maven, npm)
2. **ci**: the commit changes to the continuous integration configuration files and scripts
3. **docs**: the commit changes the documentation only
4. **feat**: the commit introduces a new feature to the codebase
5. **fix**: the commit patches a bug in your codebase
6. **perf**: the commit includes code change that improves performance
7. **refactor**: the commit contains only refactoring, so the code changes are neither fixes a bug nor adds a feature
8. **style**: the commit changes those parts that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc.)
9. **test**: the commit includes missing tests or correcting existing tests

For Linux and Windows systems, there are nine-nine templates that contains LF or CRLF line endings in the templates: it is denoted by "lf" or "crlf" in their file names.

The detailed desctioption of the templates could be found on [Git commit practices](https://blog.kese.hu/2020/10/git-commit-best-practices.html) page.  

For using this tool you have to run the following Git config alias commands in the actual Git repo:

**Template for build targeted commits**

```
git config --global alias.ci-build "commit --template=@commit-template/issue_build-template{_crlf|_lf}.gcm"
```

**Template for continuous integration targeted commits**

```
git config --global alias.ci-ci "commit --template=@commit-template/issue_ci-template{_crlf|_lf}.gcm"
```

**Template for documentation targeted commits**

```
git config --global alias.ci-docs "commit --template=@commit-template/issue_docs-template{_crlf|_lf}.gcm"
```

**Template for feature targeted commits**

```
git config --global alias.ci-feat "commit --template=@commit-template/issue_feat-template{_crlf|_lf}.gcm"
```

**Template for bug fix targeted commits**

```
git config --global alias.ci-fix "commit --template=@commit-template/issue_fix-template{_crlf|_lf}.gcm"
```

**Template for performance improvement targeted commits**

```
git config --global alias.ci-perf "commit --template=@commit-template/issue_perf-template{_crlf|_lf}.gcm"
```

**Template for refactor targeted commits**

```
git config --global alias.ci-refactor "commit --template=@commit-template/issue_refactor-template{_crlf|_lf}.gcm"
```

**Template for writing style targeted commits**

```
git config --global alias.ci-style "commit --template=@commit-template/issue_style-template{_crlf|_lf}.gcm"
```

**Template for automated test targeted commits**

```
git config --global alias.ci-test "commit --template=@commit-template/issue_test-template{_crlf|_lf}.gcm"
```

### Commit message syntax highlight

Syntax highlighting is a feature of text editors that are used for display texts in different colours and fonts according to the category of terms. Currently two editors are supported: nano and Sublime Text.

#### Sublime editor for composing messages (proposed for CMD)

It is recommended to use the popular [Sublime Text](https://www.sublimetext.com/) (ST) editor for composing commit messages in graphical user interface environments: e.g  in Windows or in Linux GNOME, KDE or xfce. (For command line parameters see the [documentation](https://www.sublimetext.com/docs/command_line.html).)

**Set Sublime Text as default editor**

```
git config --global core.editor 'subl -n -w'
```

**Set commit message syntax highlight** (*Note: MANUAL configuration is required!*)

If you use the Sublime Text editor then you may have the opportunity to highlight the keywords in the messages to increase the comfort of message composition. In this case the following `sublime-syntax` file should be copied to the Sublime's User directory (e.g. on Windows `C:\Users\<user>\AppData\Roaming\Sublime Text 3\Packages\User`):

```
Git-commit-message.sublime-syntax
```

In order to apply this syntax automatically when the Git commit aliases (e.g. `git ci-feat`) are issued on the console, you have to install and configure [ApplySyntax](https://facelessuser.github.io/ApplySyntax/) Sublime package. (As Sublime could not determine the syntax of the commit message out-of-the-box when it is composed from the initial 'COMMIT_EDITMSG' file automatically.)

*Install ApplySyntax package*

You have to install ApplySyntax package with the help of the Sublime `Package Control`.  

*Configure ApplySyntax package to use Git commit message syntax*

Copy the following configuration code snippet into the Preferences | Package Settings | ApplySyntax | Settings window within the `syntaxes` section in the User settings window:

```
"syntaxes": [
    {
        "syntax": "User/Git-commit-message",
        "extensions": ["gcm"],
        "rules": [
            {"first_line": "^(fix|feat|docs|ci|build|perf|refactor|style|test).*<description>$"}
        ]
    }
]
```

#### Nano editor for composing messages (proposed for bash)

It is recommended to use the popular [nano](https://www.nano-editor.org/) editor for composing commit messages in command line interfaces (CLI): e.g. in Linux bash or Windows Subsystem for Linux (WSL). (For command line parameters see the [documentation](https://www.nano-editor.org/docs.php).)

**Set nano Text as default editor**

```
git config --global core.editor nano
```

**Set commit message syntax highlight**

If you use the nano editor then you may have the opportunity to highlight the keywords in the messages to increase the comfort of message composition. 

```
cat @git/nano-syntax-highlight/Git-commit-message.nano-syntax >> ~/.nanorc
```

### Convention 4: Commit message rule validation

Git hooks are scripts that run automatically every time a particular event occurs in a Git repository. Hooks can reside in either local or server-side repositories, and they are only executed in response to actions in that repository.

The reason why client-side Git hooks are so powerful is that they can catch invalid commits and provide feedback about them before changes are pushed to the remote repository.

> Note: It requires Perl 5.22+. Some Perl variants are available on Windows (Strawberry Perl is recommended), but the followings instructions are not tested on these Perl variants.

 [Git::Hooks](https://metacpan.org/pod/Git::Hooks) is a framework for implementing Git hooks. Using this framework helps to configure commit message conventions on client side.

#### Install Git::Hooks framework

```
sudo cpanm System::Command --force # because tests fail...
sudo cpanm Git::Repository
sudo cpanm Git::Hooks
```

For using this tool you have to run the following Git config commands in the actual Git repo:

#### Configure CheckLog plugin

To configure commit messages check, follow the next [CheckLog](https://metacpan.org/pod/Git::Hooks::CheckLog) configuration steps.

**Enable CheckLog plugin to enforce commit message policies**

```
git config --global githooks.plugin CheckLog --add
```

###### Step 1: Set commit message title-related (i.e. head) checks

**Set commit message title existence check**

```
git config --global githooks.checklog.title-required true
```

**Set commit message title length check**

```
git config --global githooks.checklog.title-max-width 72
```

**Set denial of period in commit message title check** 

```
git config --global githooks.checklog.title-period deny
```

**Set commit message title format check**

```
git config --global githooks.checklog.title-match --replace-all '^(build|ci|docs|feat|fix|perf|refactor|style|test)(\\[)?(?(1)[A-Za-z_-]*\\]|)(\\[)?(?(2)\!\\]|):\\s.*'
```

> Note: On RegExp interpretation:
>
> ^(build|ci|docs|feat|fix|perf|refactor|style|test) 		 # title should start only with these commit types
>
> (\[)?(?(1)[A-Za-z_-]*\]|)																# specification of scope wording if there is any (optional)
>
> (\[)?(?(2)!\]|)																				 # is it a breaking change? (optional)
>
> :\\s.*																							# colon plus space then any character is allowed
>
> The rule above is derived from the commit message templates.

**Set commit message title tab denial check**

```
git config --global githooks.checklog.title-match '!\t' --add
```

**Set commit message title [scope] intact denial check**

```
git config --global githooks.checklog.title-match '!\[scope\]' --add
```

**Set commit message title <description> intact denial check**

```
git config --global githooks.checklog.title-match '!<description>' --add
```

###### Step 2: Set commit message body-related checks

**Set commit message body length check**

```
git config --global githooks.checklog.body-max-width 72
```

**Set commit message body tab denial check** 

```
git config --global --replace-all githooks.checklog.match '!\t'
```

**Set commit message body [body] intact denial check**

```
git config --global githooks.checklog.match '!\[body\]' --add
```

**Set commit message body CC intact denial check**

```
git config --global githooks.checklog.match '!CC:\s<git\susername>' --add
```

**Set commit message body Doc-impact intact denial check**

```
git config --global githooks.checklog.match '!Doc-impact:\s<document\sname>' --add
```

**Set commit message body Reviewer intact denial check**

```
git config --global githooks.checklog.match '!Reviewed-by:\s<git\susername>' --add
```

**Set commit message body Resolves intact denial check**

```
git config --global githooks.checklog.match '!Resolves:\s<JIRA\sissue\skey>' --add
```

**Set commit message body BREAKING-CHANGE intact denial check**

```
git config --global githooks.checklog.match '!BREAKING-CHANGE:\s<description>' --add
```

**Set commit message body BREAKING-CHANGE entails [!] check**

```
git config --global githooks.checklog.match '(?s)(?<=\[!\]).*BREAKING-CHANGE:\s.*' --add
```

**Set commit message body [!] entails BREAKING-CHANGE check**

```
git config --global githooks.checklog.match '(?s)\[!\].*(?=BREAKING-CHANGE:\s.*)' --add
```

#### Configure CheckJira plugin

To configure Jira issue reference check in commit messages, follow the next [CheckJira](https://metacpan.org/pod/Git::Hooks::CheckJira) configuration steps.

**Enable CheckJira plugin to check Jira issue reference in commit messages**

```
git config --global githooks.plugin CheckJira --add
```

**Set Jira API endpoint to access the referenced issues**

```
git config --global githooks.checkjira.jiraurl 'http://192.168.0.27:9081/jira/rest/api/latest'
```

> Note: Currently, the Jira endpoint is available using VPN only.

**Set Jira username to access the referenced issues**

```
git config --global githooks.checkjira.jirauser '<YOUR JIRA USERNAME>'
```

**Set Jira password to access the referenced issues**

```
git config --global githooks.checkjira.jirapass '<YOUR JIRA PASSWORD>'
```

**Set the reference check with the following pattern**

```
git config --global githooks.checkjira.matchlog '(?m)^Resolves:\\s([A-Z][A-Z]+-[0-9]+)'
```

**Set constrain on the checked Jira issue**

```
git config --global githooks.checkjira.jql 'project = NET AND issuetype IN (Story, Improvement, Bug) AND status IN (\"In progress\", \"In review\", Done)'
```

> Note: On Jira filter. 
>
> The following conditions should be met relating to the referenced Jira issue key:
>
> * Project key is 'NET'
> * Only Story, Improvement and Bug issue type is allowed
> * Only issues that are in In progress, In review or in Done status are allowed.

## Additional settings

### Setting Perforce diff and merge tools

There are many diff and merge tools are available. One of the most popular is the [Perforce diff and merge tool](https://www.perforce.com/products/helix-core-apps/merge-diff-tool-p4merge). It is used to visualize differences and to accomplish merge within codelines. To set up Perforce p4merge tool follow the [instruction](https://danlimerick.wordpress.com/2011/06/19/git-for-window-tip-use-p4merge-as-mergetool/). 

For using this tool the following Git config command should be run after installation (n.b. paths may be amended to reflect local situations):

```
# When Git tells you that there has been conflict, to resolve it type: git mergetool
git config --global merge.tool p4merge
git config --global mergetool.p4merge.path "C:/Program Files/Perforce/p4merge.exe"
git config --global mergetool.p4merge.prompt false

# For diffs call: git difftool 
git config --global diff.tool p4merge
git config --global difftool.p4merge.path "C:/Program Files/Perforce/p4merge.exe"
git config --global difftool.p4merge.prompt false
```

## License

[MIT](https://choosealicense.com/licenses/mit/)