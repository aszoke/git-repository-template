﻿Write-Host ""
Write-Host "==================================================================="
Write-Host "|                                                                 |"
Write-Host "|                       SETUP GIT CONFIG                          |"
Write-Host "|                       (for PowerShell)                          |"
Write-Host "|                                                                 |"
Write-Host "| For helping you to set some proposed basic Git configurations.  |"
Write-Host "|                                                                 |"
Write-Host "==================================================================="
Write-Host ""

# ------ Asking the setting target
Write-Host "Would you like to set user-specific (G) or repo-specific (L) settings?" -ForegroundColor DarkYellow
$isGlobal = Read-Host -Prompt 'Answer (G/L)'
if($isGlobal -ieq "g") {
   $specific = "global"
}else {
   $specific = "local"
}
Write-Host "Set target configuration as: " -NoNewLine 
Write-Host "$specific" -ForegroundColor DarkYellow 


# ===============================================
Write-Host ""
Write-Host " --==[ PHASE 1/4 ]==-- " -ForegroundColor DarkYellow

# ------ Questions and executable commands in array
$textBasic = New-Object System.Collections.Generic.List[System.Object]
$commandBasic = New-Object System.Collections.Generic.List[System.Object]

$textBasic.Add("Auto-converting CRLF to LF line endings depending on Windows?")
$commandBasic.Add("git config --$specific core.autocrlf true")

$textBasic.Add("Enable long filenames on Windows?")
$commandBasic.Add("git config --$specific core.longpaths true")

$textBasic.Add("Automatically colors Git output?")
$commandBasic.Add("git config --$specific ui.color true")

$textBasic.Add("Set UTF-8 display for Git GUI differences window?")
$commandBasic.Add("git config --$specific gui.encoding utf-8")

$textBasic.Add("Autocorrect typos: it gives a clue about the most similar command?")
$commandBasic.Add("git config --$specific help.autocorrect 1")

$textBasic.Add("Abbreviation of Git status?")
$commandBasic.Add("git config --$specific alias.st status")

$textBasic.Add("Abbreviation of Git commit?")
$commandBasic.Add("git config --$specific alias.ci commit")

$textBasic.Add("Abbreviation of Git checkout?")
$commandBasic.Add("git config --$specific alias.co checkout")

$textBasic.Add("Abbreviation of Git push to master?")
$commandBasic.Add("git config --$specific alias.pom 'push origin master'")

$textBasic.Add("History graphs?")
$commandBasic.Add("git config --$specific alias.hist 'log --all --graph --decorate --oneline'")

$textBasic.Add("Pretty Git branch graphs?")
$commandBasic.Add("git config --$specific alias.show-graph 'log --graph --abbrev-commit --pretty=oneline'")

# $textBasic.Add("XXX")
# $commandBasic.Add("XXX")

# ------ Evaluating user answers
$again = $true
$i=0
while ( $again ) {

    Write-Host ""
    if ( $i -lt $textBasic.Count ) {
        Write-Host "QUESTION #$($i+1) : " -NoNewline -ForegroundColor DarkYellow
        Write-Host $textBasic[$i]
        Write-Host "(Note, it will execute: " $commandBasic[$i] ")"
        $isSet = Read-Host -Prompt 'Answer (Y/N/Q)'
        if($isSet -ieq "y") {
            Invoke-Expression $commandBasic[$i]
            Write-Host "Done." -ForegroundColor Green
        } elseif($isSet -ieq "q") {
            Write-Host "Setting skipped." -ForegroundColor Yellow
            $again = $false
            Write-Host ""
        } else {
            Write-Host "Skipped." -ForegroundColor Yellow
        }

    } else {
        $again = $false
    }

    $i++
}


# ===============================================
Write-Host " --==[ PHASE 2/4 ]==-- " -ForegroundColor DarkYellow
Write-Host ""

# ------ Evaluating user answers
Write-Host "Would you like to set standardized commit messages?" -ForegroundColor DarkYellow
$isStandardCommitMessage = Read-Host -Prompt 'Answer (Y/N)'
Write-Host ""

if($isStandardCommitMessage -ieq "y") {

    Write-Host "Will you use Bash (B) or CMD (C) (to set proper line endings in templates)?" -ForegroundColor DarkYellow
    $isBash = Read-Host -Prompt 'Answer (B/C)'

    if($isBash -ieq "b") {
      $system="Unix (LF)"
      $le="lf"
    } else {
      $system="Windows (CRLF)"  
      $le="crlf"
    }
    Write-Host "SET " -ForegroundColor DarkYellow -NoNewline
    Write-Host "File line endings to commit messages: " -NoNewline
    Write-Host "$system"

}

# ------ Questions and executable commands in array
$textCommit = New-Object System.Collections.Generic.List[System.Object]
$commandCommit = New-Object System.Collections.Generic.List[System.Object]

$textCommit.Add("Template for build targeted commits")
$commandCommit.Add("git config --$SPECIFIC alias.ci-build 'commit --template=@git/commit-template/issue_build-template_$le.gcm'")

$textCommit.Add("Template for continuous integration targeted commits")
$commandCommit.Add("git config --$SPECIFIC alias.ci-ci 'commit --template=@git/commit-template/issue_ci-template_$le.gcm'")

$textCommit.Add("Template for documentation targeted commits")
$commandCommit.Add("git config --$SPECIFIC alias.ci-docs 'commit --template=@git/commit-template/issue_docs-template_$le.gcm'")

$textCommit.Add("Template for feature targeted commits")
$commandCommit.Add("git config --$SPECIFIC alias.ci-feat 'commit --template=@git/commit-template/issue_feat-template_$le.gcm'")

$textCommit.Add("Template for bug fix targeted commits")
$commandCommit.Add("git config --$SPECIFIC alias.ci-fix 'commit --template=@git/commit-template/issue_fix-template_$le.gcm'")

$textCommit.Add("Template for performance improvement targeted commits")
$commandCommit.Add("git config --$SPECIFIC alias.ci-perf 'commit --template=@git/commit-template/issue_perf-template_$le.gcm'")

$textCommit.Add("Template for refactor targeted commits")
$commandCommit.Add("git config --$SPECIFIC alias.ci-refactor 'commit --template=@git/commit-template/issue_refactor-template_$le.gcm'")

$textCommit.Add("Template for writing style targeted commits")
$commandCommit.Add("git config --$SPECIFIC alias.ci-style 'commit --template=@git/commit-template/issue_style-template_$le.gcm'")

$textCommit.Add("Template for automated test targeted commits")
$commandCommit.Add("git config --$SPECIFIC alias.ci-test 'commit --template=@git/commit-template/issue_test-template_$le.gcm'")

# $textCommit.Add("")
# $commandCommit.Add("")

if($isStandardCommitMessage -ieq "y") {
   for ($k=0; $k -lt $textCommit.Count; $k++) {
        Write-Host ""
        Write-Host "SET #$($k+1) :"  -ForegroundColor DarkYellow -NoNewline
        Write-Host $textCommit[$k]
        Invoke-Expression $commandCommit[$k]
        Write-Host $commandCommit[$k] -NoNewline
        Write-Host " ... Done." -ForegroundColor Green
   }
}
else {
    Write-Host "Setting skipped." -ForegroundColor Yellow
}

# ===============================================
Write-Host ""
Write-Host " --==[ PHASE 3/4 ]==-- " -ForegroundColor DarkYellow
Write-Host ""

# ------ Evaluating user answers
Write-Host "Set default editor to compose commit messages:" -ForegroundColor DarkYellow
Write-Host " (N) - nano    (for Bash environment)"
Write-Host " (S) - sublime (for CMD environment)"
Write-Host " (Q) - to quit the question"

$editor = Read-Host -Prompt 'Answer (N/S/Q)'
if($editor -ieq "n") {

    Write-Host "Checking whether nano is installed: " -ForegroundColor DarkYellow 

    try {
        Invoke-Expression "nano -V"
        Write-Host "OK - nano is installed." -ForegroundColor DarkYellow
        $isNanoInstalled=$true
    } catch {
        Write-Host "ERROR: nano is not installed! Nano will not be set as default editor for commit messages!" -ForegroundColor Red
        $isNanoInstalled=$false
    }

    if($isNanoInstalled -eq $true) {
        Write-Host "SET "  -ForegroundColor DarkYellow -NoNewline
        Write-Host "Nano as default text editor for Git" -NoNewline
        Invoke-Expression "git config --$specific core.editor 'nano'"
        Write-Host " ... Done." -ForegroundColor Green 
    }
}
elseif ($editor -ieq "s") {

    Write-Host "Checking whether sublime is installed: " -ForegroundColor DarkYellow 

    try {
        Invoke-Expression "subl -v"
        Write-Host "OK - sublime is installed." -ForegroundColor DarkYellow
        $isSublimeInstalled=$true
    } catch {
        Write-Host "ERROR: sublime is not installed! Sublime will not be set as default editor for commit messages!" -ForegroundColor Red
        $isSublimeInstalled=$false
    }

    if($isSublimeInstalled -eq $true) {
        Write-Host "SET "  -ForegroundColor DarkYellow -NoNewline
        Write-Host "Sublime as default text editor for Git" -NoNewline
        Invoke-Expression "git config --$specific core.editor 'subl -n -w'"
        Write-Host " ... Done." -ForegroundColor Green 
    }
} 
else {
    Write-Host "Setting skipped." -ForegroundColor Yellow
}


# ===============================================
Write-Host ""
Write-Host " --==[ PHASE 4/4 ]==-- " -ForegroundColor DarkYellow
Write-Host ""

# ------ Evaluating user answers
Write-Host "Would you like to set commit message validation (PERL IS REQUIRED!)?" -ForegroundColor DarkYellow
$isCommitMessageValidation = Read-Host -Prompt 'Answer (Y/N)'
Write-Host ""

if($isCommitMessageValidation -ieq "y") {

    Write-Host "Checking whether Perl is installed: " -ForegroundColor DarkYellow 

    # Checking Perl installation
    try {
        Invoke-Expression "perl -v"
        Write-Host "OK - Perl is installed." -ForegroundColor DarkYellow
        $isPerlInstalled=$true

        Write-Host "For Jira access give your credentials:" -ForegroundColor DarkYellow
        $jiraUser = Read-Host -Prompt 'Jira username'
        $jiraPass = Read-Host -Prompt 'Jira password'
    } catch {
        Write-Host "ERROR: Perl is not installed! Commit message validation will not be set!" -ForegroundColor Red
        $isPerlInstalled=$false
    }
}

# ------ Questions and executable commands in array
$textGitHooks = New-Object System.Collections.Generic.List[System.Object]
$commandGitHooks = New-Object System.Collections.Generic.List[System.Object]

$textGitHooks.Add("Enable CheckLog plugin to enforce commit message policies")
$commandGitHooks.Add("git config --$SPECIFIC --replace-all githooks.plugin CheckLog")

# ----- Checklog-related title settings

$textGitHooks.Add("Set commit message title existence check")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checklog.title-required true")

$textGitHooks.Add("Set commit message title length check")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checklog.title-max-width 72")

$textGitHooks.Add("Set commit message title period denial check")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checklog.title-period deny")

# coditional regex: http://www.rexegg.com/regex-conditionals.html
# I.e. if there is an opening [ then should be a closing ]
$textGitHooks.Add("Set commit message title format check")
$commandGitHooks.Add("git config --$SPECIFIC --replace-all githooks.checklog.title-match '^(build|ci|docs|feat|fix|perf|refactor|style|test)(\[)?(?(1)[A-Za-z_-]*\]|)(\[)?(?(2)\!\]|):\s.*'")

$textGitHooks.Add("Set commit message title tab denial check")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checklog.title-match '!\t' --add")

$textGitHooks.Add("Set commit message title [scope] intact denial check")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checklog.title-match '!\[scope\]' --add")

$textGitHooks.Add("Set commit message title <description> intact denial check")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checklog.title-match '!<description>' --add")

# ----- Checklog-related body settings

$textGitHooks.Add("Set commit message body length check")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checklog.body-max-width 72")

$textGitHooks.Add("Set commit message body tab denial check")
$commandGitHooks.Add("git config --$SPECIFIC --replace-all githooks.checklog.match '!\t'")

$textGitHooks.Add("Set commit message body [body] intact denial check")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checklog.match '!\[body\]' --add")

$textGitHooks.Add("Set commit message body CC intact denial check")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checklog.match '!CC:\s<git\susername>' --add")

$textGitHooks.Add("Set commit message body Doc-impact intact denial check")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checklog.match '!Doc-impact:\s<document\sname>' --add")

$textGitHooks.Add("Set commit message body Reviewer intact denial check")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checklog.match '!Reviewed-by:\s<git\susername>' --add")

$textGitHooks.Add("Set commit message body Resolves intact denial check")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checklog.match '!Resolves:\s<JIRA\sissue\skey>' --add")

$textGitHooks.Add("Set commit message body BREAKING-CHANGE intact denial check")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checklog.match '!BREAKING-CHANGE:\s<description>' --add")

# lookbefore regex: http://www.rexegg.com/regex-lookarounds.html
# I.e. if there is BREAKING-CHANGES then [!] must be before it.
$textGitHooks.Add("Set commit message body BREAKING-CHANGE entails [!] check")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checklog.match '(?s)(?<=\[!\]).*BREAKING-CHANGE:\s.*' --add")

# lookahead regex: http://www.rexegg.com/regex-lookarounds.html
# I.e. if there is [!] then BREAKING-CHANGES  must be after it.
$textGitHooks.Add("Set commit message body [!] entails BREAKING-CHANGE check")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checklog.match '(?s)\[!\].*(?=BREAKING-CHANGE:\s.*)' --add")

# ----- Checkjira-relates settings

$textGitHooks.Add("Enable CheckJira plugin to check Jira issue reference in commit messages")
$commandGitHooks.Add("git config --$SPECIFIC githooks.plugin CheckJira --add")

$textGitHooks.Add("Set Jira API endpoint to access the referenced issues")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checkjira.jiraurl 'http://192.168.0.27:9081/jira/rest/api/latest'")

$textGitHooks.Add("Set Jira username to access the referenced issues")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checkjira.jirauser '$jiraUser'")

$textGitHooks.Add("Set Jira password to access the referenced issues")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checkjira.jirapass '$jiraPass'")

$textGitHooks.Add("Set the reference check with the following pattern")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checkjira.matchlog '(?m)^Resolves:\s([A-Z][A-Z]+-[0-9]+)'")

$textGitHooks.Add("Set constrain on the checked Jira issue")
$commandGitHooks.Add("git config --$SPECIFIC githooks.checkjira.jql 'issuetype IN (Story, Improvement, Bug) AND status IN (\`"In progress\`", \`"In review\`", Done)'")

# $textGitHooks.Add("")
# $commandGitHooks.Add("")

if($isCommitMessageValidation -ieq "y" -And $isPerlInstalled -eq $true) {

   for ($k=0; $k -lt $textGitHooks.Count; $k++) {
        Write-Host ""
        Write-Host "SET #$($k+1) : " -ForegroundColor DarkYellow -NoNewline
        Write-Host $textGitHooks[$k]
        Invoke-Expression $commandGitHooks[$k]
        Write-Host $commandGitHooks[$k] -NoNewline
        Write-Host " ... Done." -ForegroundColor Green
   }
}
else {
    Write-Host "Setting skipped." -ForegroundColor Yellow
}

Write-Host ""
Write-Host "Woohoo...no more questions. ._.)/\(._. Bye." -ForegroundColor DarkYellow
Exit
