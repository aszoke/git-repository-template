#!/bin/bash

echo
echo "==================================================================="
echo "|                                                                 |"
echo "|                       SETUP GIT CONFIG                          |"
echo "|                          (for bash)                             |"
echo "|                                                                 |"
echo "| For helping you to set some proposed basic Git configurations.  |"
echo "|                                                                 |"
echo "==================================================================="
echo

# Color constants
RED='\033[0;31m'
GREEN='\033[32m'
LIGHTGREEN='\033[92m'
YELLOW='\033[33m'
LIGHTYELLOW='\033[93m'
IWHITE='\033[97m'
NC='\033[0m' # No Color
DQT='"' # escaping double quote

# ------ Asking the setting target
echo -e "${YELLOW}Would you like to set user-specific (G) or repo-specific (L) settings?${NC}"
SPECIFIC="local"
read -p "Answer (G/L): " ISGLOBAL

if [[ $ISGLOBAL = "G" || $ISGLOBAL = "g" ]];
then
    SPECIFIC="global"
fi
echo -e "${YELLOW}SET${NC} target configuration as: ${IWHITE}$SPECIFIC${NC}"

# ===============================================
echo
echo -e "${YELLOW} --==[ PHASE 1/4 ]==-- ${NC}"

# ------ Questions and executable commands in array
i=1
text_basic[$i]="Auto-converting CRLF to LF line endings depending on Windows?"
command_basic[$i]="git config --$SPECIFIC core.autocrlf true"

((i=i+1))
text_basic[$i]="Enable long filenames on Windows?"
command_basic[$i]="git config --$SPECIFIC core.longpaths true"

((i=i+1))
text_basic[$i]="Automatically colors Git output?"
command_basic[$i]="git config --$SPECIFIC ui.color true"

((i=i+1))
text_basic[$i]="Set UTF-8 display for Git GUI differences window?"
command_basic[$i]="git config --$SPECIFIC gui.encoding utf-8"

((i=i+1))
text_basic[$i]="Autocorrect typos: it gives a clue about the most similar command?"
command_basic[$i]="git config --$SPECIFIC help.autocorrect 1"

((i=i+1))
text_basic[$i]="Abbreviation of Git status?"
command_basic[$i]="git config --$SPECIFIC alias.st status"

((i=i+1))
text_basic[$i]="Abbreviation of Git commit?"
command_basic[$i]="git config --$SPECIFIC alias.ci commit"

((i=i+1))
text_basic[$i]="Abbreviation of Git checkout?"
command_basic[$i]="git config --$SPECIFIC alias.co checkout"

((i=i+1))
text_basic[$i]="Abbreviation of Git push to master?"
command_basic[$i]="git config --$SPECIFIC alias.pom 'push origin master'"

((i=i+1))
text_basic[$i]="History graphs?"
command_basic[$i]="git config --$SPECIFIC alias.hist 'log --all --graph --decorate --oneline'"

((i=i+1))
text_basic[$i]="Pretty Git branch graphs?"
command_basic[$i]="git config --$SPECIFIC alias.show-graph 'log --graph --abbrev-commit --pretty=oneline'"

# ((i=i+1))
# text_basic[$i]="XXX"
# command_basic[$i]="XXX"

# ------ Evaluating user answers
AGAIN="true"
ii=1
while [[ "$AGAIN" = "true" ]]; do

    echo
  	if [[ "${ii}" -le "${i}" ]]; then
    	echo -e "${YELLOW}QUESTION $ii${NC}: ${text_basic[$ii]}"
    	echo "(Note, it will execute: ${command_basic[$ii]})"
    	read -p "Answer (Y/N/Q): " ISSET
    	if [[ $ISSET = "Y" || $ISSET = "y" ]];
		  then
    		eval ${command_basic[$ii]}
    		echo -e "${LIGHTGREEN}Done.${NC}"
		  elif [[ $ISSET = "Q" || $ISSET = "q" ]]; then 
        echo -e "${LIGHTYELLOW}Setting skipped.${NC}"
			  ((AGAIN="false"))
        echo
		  else
			  echo -e "${LIGHTYELLOW}Skipped.${NC}"
		  fi
  	else
  		  ((AGAIN="false"))
  	fi
  	((i++))
done


# ===============================================
echo -e "${YELLOW} --==[ PHASE 2/4 ]==-- ${NC}"
echo

# ------ Evaluating user answers
echo -e "${YELLOW}Would you like to set standardized commit messages?${NC}"
read -p "Answer (Y/N): " IS_STANDARD_COMMIT_MESSAGE
echo

if [[ $IS_STANDARD_COMMIT_MESSAGE = "Y" || $IS_STANDARD_COMMIT_MESSAGE = "y" ]];
then

    echo -e "${YELLOW}Will you use Bash (B) or CMD (C) - to set proper line endings in templates?${NC}"
    read -p "Answer (B/C): " IS_BASH

    if [[ $IS_BASH = "B" || $IS_BASH = "b" ]]; then
      SYSTEM="Unix (LF)"
      LE="lf"
    else
      SYSTEM="Windows (CRLF)"  
      LE="crlf"
    fi
    echo -e "${YELLOW}SET${NC} File line endings to commit messages: ${IWHITE}$SYSTEM${NC}"
    echo

fi

# ------ Questions and executable commands in array
j=1
text_commit[$j]="Template for build targeted commits"
command_commit[$j]="git config --$SPECIFIC alias.ci-build 'commit --template=@git/commit-template/issue_build-template_$LE.gcm'"

((j=j+1))
text_commit[$j]="Template for continuous integration targeted commits"
command_commit[$j]="git config --$SPECIFIC alias.ci-ci 'commit --template=@git/commit-template/issue_ci-template_$LE.gcm'"

((j=j+1))
text_commit[$j]="Template for documentation targeted commits"
command_commit[$j]="git config --$SPECIFIC alias.ci-docs 'commit --template=@git/commit-template/issue_docs-template_$LE.gcm'"

((j=j+1))
text_commit[$j]="Template for feature targeted commits"
command_commit[$j]="git config --$SPECIFIC alias.ci-feat 'commit --template=@git/commit-template/issue_feat-template_$LE.gcm'"

((j=j+1))
text_commit[$j]="Template for bug fix targeted commits"
command_commit[$j]="git config --$SPECIFIC alias.ci-fix 'commit --template=@git/commit-template/issue_fix-template_$LE.gcm'"

((j=j+1))
text_commit[$j]="Template for performance improvement targeted commits"
command_commit[$j]="git config --$SPECIFIC alias.ci-perf 'commit --template=@git/commit-template/issue_perf-template_$LE.gcm'"

((j=j+1))
text_commit[$j]="Template for refactor targeted commits"
command_commit[$j]="git config --$SPECIFIC alias.ci-refactor 'commit --template=@git/commit-template/issue_refactor-template_$LE.gcm'"

((j=j+1))
text_commit[$j]="Template for writing style targeted commits"
command_commit[$j]="git config --$SPECIFIC alias.ci-style 'commit --template=@git/commit-template/issue_style-template_$LE.gcm'"

((j=j+1))
text_commit[$j]="Template for automated test targeted commits"
command_commit[$j]="git config --$SPECIFIC alias.ci-test 'commit --template=@git/commit-template/issue_test-template_$LE.gcm'"

# ((j=j+1))
# text_commit[$j]=""
# command_commit[$j]=""

if [[ $IS_STANDARD_COMMIT_MESSAGE = "Y" || $IS_STANDARD_COMMIT_MESSAGE = "y" ]];
then
    for (( jj=1; jj<=$j; jj++ ))
    do 
      echo -e "${YELLOW}SET #$jj${NC}: ${text_commit[$jj]}"
      eval ${command_commit[$jj]}
      echo -e "${command_commit[$jj]} ... ${LIGHTGREEN}Done.${NC}"
      echo
    done
else
  echo -e "${LIGHTYELLOW}Setting skipped.${NC}"
fi

# ===============================================
echo -e "${YELLOW} --==[ PHASE 3/4 ]==-- ${NC}"
echo

# ------ Evaluating user answers
echo -e "${YELLOW}Set default editor to compose commit messages:${NC}"
echo " (N) - nano    (for Bash environment)"
echo " (S) - sublime (for CMD environment)"
echo " (Q) - to quit the question"
read -p "Answer (N/S/Q): " EDITOR

if [[ $EDITOR = "N" || $EDITOR = "n" ]]; 
then

    echo -e "${YELLOW}Checking whether nano is installed: ${NC}"

    if [[ $(command -v nano) ]];
    then
      echo -e "${YELLOW}OK - nano is installed.${NC}"
      IS_NANO_INSTALLED="y"
    else
      echo -e "${RED}ERROR: nano is not installed! Nano will not be set as default editor for commit messages!${NC}"
      IS_NANO_INSTALLED="n"      
    fi

    if [[ $IS_NANO_INSTALLED = "y" ]];
    then
      eval "git config --$SPECIFIC core.editor 'nano'"
      echo -e "${YELLOW}SET${NC} Nano as default text editor for Git ...${LIGHTGREEN} Done.${NC}"

      eval "cat @git/nano-syntax-highlight/Git-commit-message.nano-syntax >> ~/.nanorc"
      echo -e "${YELLOW}SET${NC} Nano syntax highlight for commit messages ...${LIGHTGREEN} Done.${NC}"
    fi

elif [[ $EDITOR = "S" || $EDITOR = "s" ]]; 
then

    echo -e "${YELLOW}Checking whether Sublime is installed: ${NC}"

    if [[ $(command -v subl) ]];
    then
      echo -e "${YELLOW}OK - Sublime is installed.${NC}"
      IS_SUBLIME_INSTALLED="y"
    else
      echo -e "${RED}ERROR: Sublime is not installed! Sublime will not be set as default editor for commit messages!${NC}"
      IS_SUBLIME_INSTALLED="n"      
    fi

    if [[ $SUBLIME = "y" ]]; then
      eval "git config --$SPECIFIC core.editor 'subl -n -w'"
      echo -e "${YELLOW}SET${NC} Sublime as default text editor for Git ...${LIGHTGREEN} Done.${NC}"
    fi

else
    echo -e "${LIGHTYELLOW}Setting skipped.${NC}"
fi

# ===============================================
echo ""
echo -e "${YELLOW} --==[ PHASE 4/4 ]==-- ${NC}"
echo ""

# ------ Evaluating user answers
echo -e "${YELLOW}Would you like to set commit message validation (PERL IS REQUIRED!)?${NC}"
read -p "Answer (Y/N): " IS_COMMIT_MESSAGE_VALIDATION

if [[ $IS_COMMIT_MESSAGE_VALIDATION = "Y" || $IS_COMMIT_MESSAGE_VALIDATION = "y" ]]; 
then

  echo -e "${YELLOW}Checking whether Perl is installed: ${NC}"

  if [[ $(command -v perl) ]];
  then
    echo -e "${YELLOW}OK - Perl is installed.${NC}"
    IS_PERL_INSTALLED="y"

    echo -e "${YELLOW}For Jira access give your credentials:${NC}"
    read -p "Jira username: " JIRA_USER
    read -p "Jira password: " JIRA_PASS
  else
    echo -e "${RED}ERROR: Perl is not installed! Commit message validation will not be set!${NC}"
    IS_PERL_INSTALLED="n"      
  fi
fi

# ------ Questions and executable commands in array
k=1

((k=k+1))
text_git_hooks[$k]="Enable CheckLog plugin to enforce commit message policies"
command_git_hooks[$k]="git config --$SPECIFIC --replace-all githooks.plugin CheckLog"

# ----- Checklog-related title settings

((k=k+1))
text_git_hooks[$k]="Set commit message title existence check"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checklog.title-required true"

((k=k+1))
text_git_hooks[$k]="Set commit message title length check"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checklog.title-max-width 72"

((k=k+1))
text_git_hooks[$k]="Set commit message title period denial check"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checklog.title-period deny"

# coditional regex: http://www.rexegg.com/regex-conditionals.html
# I.e. if there is an opening [ then should be a closing ]
((k=k+1))
text_git_hooks[$k]="Set commit message title format check"
command_git_hooks[$k]="git config --$SPECIFIC --replace-all githooks.checklog.title-match '^(build|ci|docs|feat|fix|perf|refactor|style|test)(\[)?(?(1)[A-Za-z_-]*\]|)(\[)?(?(2)\!\]|):\s.*'"

((k=k+1))
text_git_hooks[$k]="Set commit message title tab denial check"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checklog.title-match '!\t' --add"

((k=k+1))
text_git_hooks[$k]="Set commit message title [scope] intact denial check"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checklog.title-match '!\[scope\]' --add"

((k=k+1))
text_git_hooks[$k]="Set commit message title <description> intact denial check"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checklog.title-match '!<description>' --add"

# ----- Checklog-related body settings

((k=k+1))
text_git_hooks[$k]="Set commit message body length check"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checklog.body-max-width 72"

((k=k+1))
text_git_hooks[$k]="Set commit message body tab denial check"
command_git_hooks[$k]="git config --$SPECIFIC --replace-all githooks.checklog.match '!\t'"

((k=k+1))
text_git_hooks[$k]="Set commit message body [body] intact denial check"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checklog.match '!\[body\]' --add"

((k=k+1))
text_git_hooks[$k]="Set commit message body CC intact denial check"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checklog.match '!CC:\s<git\susername>' --add"

((k=k+1))
text_git_hooks[$k]="Set commit message body Doc-impact intact denial check"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checklog.match '!Doc-impact:\s<document\sname>' --add"

((k=k+1))
text_git_hooks[$k]="Set commit message body Reviewer intact denial check"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checklog.match '!Reviewed-by:\s<git\susername>' --add"

((k=k+1))
text_git_hooks[$k]="Set commit message body Resolves intact denial check"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checklog.match '!Resolves:\s<JIRA\sissue\skey>' --add"

((k=k+1))
text_git_hooks[$k]="Set commit message body BREAKING-CHANGE intact denial check"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checklog.match '!BREAKING-CHANGE:\s<description>' --add"

# lookbefore regex: http://www.rexegg.com/regex-lookarounds.html
# I.e. if there is BREAKING-CHANGES then [!] must be before it.
((k=k+1))
text_git_hooks[$k]="Set commit message body BREAKING-CHANGE entails [!] check"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checklog.match '(?s)(?<=\[!\]).*BREAKING-CHANGE:\s.*' --add"

# lookahead regex: http://www.rexegg.com/regex-lookarounds.html
# I.e. if there is [!] then BREAKING-CHANGES  must be after it.
((k=k+1))
text_git_hooks[$k]="Set commit message body [!] entails BREAKING-CHANGE check"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checklog.match '(?s)\[!\].*(?=BREAKING-CHANGE:\s.*)' --add"

# ----- Checkjira-relates settings

((k=k+1))
text_git_hooks[$k]="Enable CheckJira plugin to check Jira issue reference in commit messages"
command_git_hooks[$k]="git config --$SPECIFIC githooks.plugin CheckJira --add"

((k=k+1))
text_git_hooks[$k]="Set Jira API endpoint to access the referenced issues"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checkjira.jiraurl 'http://192.168.0.27:9081/jira/rest/api/latest'"

((k=k+1))
text_git_hooks[$k]="Set Jira username to access the referenced issues"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checkjira.jirauser '$JIRA_USER'"

((k=k+1))
text_git_hooks[$k]="Set Jira password to access the referenced issues"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checkjira.jirapass '$JIRA_PASS'"

((k=k+1))
text_git_hooks[$k]="Set the reference check with the following pattern"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checkjira.matchlog '(?m)^Resolves:\s([A-Z][A-Z]+-[0-9]+)'"

((k=k+1))
text_git_hooks[$k]="Set constrain on the checked Jira issue"
command_git_hooks[$k]="git config --$SPECIFIC githooks.checkjira.jql 'issuetype IN (Story, Improvement, Bug) AND status IN (${DQT}In progress${DQT}, ${DQT}In review${DQT}, Done)'"

# ((k=k+1))
# text_git_hooks[$k]=""
# command_git_hooks[$k]=""

if [[ $IS_COMMIT_MESSAGE_VALIDATION = "y" || $IS_COMMIT_MESSAGE_VALIDATION = "y" ]]; 
then
  if [[ $IS_PERL_INSTALLED = "y" ]];
  then
    for (( kk=1; kk<=$k; kk++ ))
    do 
      echo -e "${YELLOW}SET #$kk${NC}: ${text_git_hooks[$kk]}"
      eval ${command_git_hooks[$kk]}
      echo -e "${command_git_hooks[$kk]} ... ${LIGHTGREEN}Done.${NC}"
      echo
    done

  fi
else
  echo -e "${LIGHTYELLOW}Setting skipped.${NC}"
fi

echo 
echo -e "${YELLOW}Woohoo...no more questions. ._.)/\(._. Bye.${NC}"
exit
