# Project name

<project name> is a `<utility/tool/feature>` that allows `<insert target audience>` to do `<action / task it does>`.

<!-- Additional lines of information text about what the project does. -->

## Prerequisites

<!-- OS / Framework / Knowledge see above -->

## Installation

To install <project name>, follow these steps:

Linux:
```
<install command>
```

Windows:
```
<install command>
```
## Usage

The project documentation could be found on the next pages: 

<!-- link to the HTML project documentation that is generated from code (e.g. with JavaDoc, Swagger doc) -->

To use <project name>, follow these steps:

```
<usage example>
```

## Build

The next frameworks and tools are required to build the project:

* <Tool 1>
* <Tool 2>
* <Tool 3>

### Steps

The following steps are required to build the project:

```
<detailed building steps>
```

## Self-test

The following tests ensure that the code is healthy:

```
<test case examples for CI>
```


## Deployment

The following steps are required to deploy the project:

```
<detailed deployment steps>
```

## Authors

The next people were the contributors of the project.

* **<person>** (project owner)
* *<person>* 

